package main

import (
  "bitbucket.org/davdima/msr-image/image"
  "net/http"
  "github.com/labstack/echo"
  mw "github.com/labstack/echo/middleware"
  "flag"
  "github.com/labstack/echo/engine/standard"
)

type httpResponse struct {
  Success bool        `json:"success"`
  Data    interface{} `json:"data"`
}

var port string

func init() {
  flag.StringVar(&port, "p", "3000", "port orf the server")
}

func handleImages(c echo.Context) error {
  imageUrl := c.QueryParam("image")
  size := c.QueryParam("size")

  if len(imageUrl) == 0 {
    return c.NoContent(http.StatusBadRequest)
  }

  img := image.NewImage(imageUrl)

  var filename string

  switch size {
  case "full":
    filename = image.FileFullSise
  case "medium":
    filename = image.FileMiddleSise
  case "small":
    filename = image.FileSmallSize
  default:
    filename = image.FileMiddleSise
  }

  img.PreCompile()
  return c.File(img.GetNamedPath(filename));
}

func main() {
  flag.Parse()
  e := echo.New()

  e.Use(mw.Recover())

  e.Get("/", handleImages)

  e.Run(standard.New(":" + port))
}
