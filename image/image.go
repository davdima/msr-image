package image

import (
	"crypto/md5"
	"fmt"
	"github.com/nfnt/resize"
	"github.com/oliamb/cutter"
	"image"
	"image/jpeg"
	_ "image/png"
  _ "image/gif"
	"io"
	"log"
	"net/http"
	"os"
	"path/filepath"
	"sync"
)

const originalImageFileName = "original"
const FileFullSise = "full"
const FileMiddleSise = "middle"
const FileSmallSize = "small"

type Image struct {
	url          string
	hashedString string
	DirName      string
}

var destinationMediaFolder string

func init() {
	destinationMediaFolder = "./img"

	makeDir(destinationMediaFolder)
}

func NewImage(imageUrl string) *Image {
	//	_ , err := url.Parse(imageUrl);
	//	panic(fmt.Sprintf("%#v", err))

	//	panic("here")
	hash := md5.New()
	fmt.Fprintf(hash, imageUrl)
	img := &Image{}

	img.hashedString = fmt.Sprintf("%x", hash.Sum(nil))
	img.url = imageUrl

	img.createDirs()
	img.downloadOriginal()
	return img
}

func (i Image) getOriginalFileExt() string {
	return filepath.Ext(i.url)
}

func (i *Image) createDirs() {

	i.DirName = fmt.Sprintf("%s/%s/%s", destinationMediaFolder, i.hashedString[0:2], i.hashedString)
	mustMakeDir(i.DirName)

}

func (i Image) GetNamedPath(fileName string) string {
	return fmt.Sprintf("%s/%s%s", i.DirName, fileName, i.getOriginalFileExt())
}

func (i Image) downloadOriginal() {
	originalFilePath := i.GetNamedPath(originalImageFileName)
	if fileNotExists(originalFilePath) {
		log.Printf("Downloading original file %s\n", i.url)
		originalImage, err := os.Create(i.GetNamedPath(originalImageFileName))
		defer originalImage.Close()
		if err != nil {
			log.Panic(err)
		}

		foreignFile, err := http.Get(i.url)
		defer foreignFile.Body.Close()

		if err != nil {
			log.Panic(err)
		}

		if _, err := io.Copy(originalImage, foreignFile.Body); err != nil {
			log.Panic(err)
		}
	}
}

func (i Image) hasFull() bool {
	return fileExists(i.GetNamedPath(FileFullSise))
}

func (i Image) hasMedium() bool {
	return fileExists(i.GetNamedPath(FileMiddleSise))
}

func (i Image) hasSmall() bool {
	return fileExists(i.GetNamedPath(FileSmallSize))
}

func (i Image) PreCompile() {
	if !i.hasFull() {
		i.compileFull()
	}

	var wg sync.WaitGroup
	if !i.hasMedium() {
		wg.Add(1)
		go i.compileMedium(&wg)
	}

	if !i.hasSmall() {
		wg.Add(1)
		go i.compileSmall(&wg)
	}

	wg.Wait()
}

func (i Image) compileFull() {
	if fileNotExists(i.GetNamedPath(originalImageFileName)) {
		i.downloadOriginal()
	}
	originalFile, err := os.Open(i.GetNamedPath(originalImageFileName))
	if err != nil {
		panic(err)
	}
	defer originalFile.Close()

	img, _, err := image.Decode(originalFile)
	if err != nil {
		panic(i.url)
	}

	i.processResizeAndCrop(img, 1030, 986, FileFullSise)

}

func (i Image) compileMedium(w *sync.WaitGroup) {
	defer w.Done()

	originalFile, err := os.Open(i.GetNamedPath(FileFullSise))
	if err != nil {
		panic(err)
	}
	defer originalFile.Close()

	img, _, err := image.Decode(originalFile)
	if err != nil {
		panic(err)
	}

	i.processResize(img, 460, 440, FileMiddleSise)
}

func (i Image) compileSmall(w *sync.WaitGroup) {
	defer w.Done()

	originalFile, err := os.Open(i.GetNamedPath(FileFullSise))
	if err != nil {
		panic(err)
	}
	defer originalFile.Close()

	img, _, err := image.Decode(originalFile)
	if err != nil {
		panic(err)
	}

	i.processResize(img, 258, 245, FileSmallSize)
}

func (i Image) processResizeAndCrop(img image.Image, width uint, height uint, destinationFileName string) {

  defer func(){
    if e := recover(); e != nil {
      log.Print(e)
    }
  }()

	if img.Bounds().Dx() <= img.Bounds().Dy() {
		width, height = height, width
	} else {
		width, height = width, height
	}

	// fmt.Printf("\n bound width: %d | bound height: %d\n", img.Bounds().Dx(), img.Bounds().Dy())
	// fmt.Printf("\n width: %d | height: %d\n", width, height)

	resized := resize.Resize(0, height, img, resize.Bicubic)

	// newWidth, newHeight := getNewDemention(int(width), int(height), resized.Bounds().Dx(), resized.Bounds().Dy())

	//fmt.Printf("\n width: %d | height: %d\n", resized.Bounds().Dx(), resized.Bounds().Dy())
	// resized = resize.Resize(uint(newWidth), uint(newHeight), img, resize.Bilinear)

	cropped, err := cutter.Crop(resized, cutter.Config{
		Width:   int(width),
		Height:  int(height),
		Mode:    cutter.Centered,
		Options: cutter.Copy,
	})

	if err != nil {
		panic(err)
	}

	destination, err := os.Create(i.GetNamedPath(destinationFileName))
	if err != nil {
		panic(err)
	}
	defer destination.Close()

	jpeg.Encode(destination, cropped, &jpeg.Options{Quality: 70})

}

func getNewDemention(mustWidth, mustHeight, exactWidth, exactHeight int) (int, int) {
	ratio := float64(exactWidth) / float64(exactHeight)
	var newHeight, newWidth float64
	if exactHeight > mustHeight {
		newHeight = float64(mustHeight)
		fmt.Printf("\n\n new height = %#v\n\n", newHeight)
		newWidth = ratio * newHeight
		fmt.Printf("\n\n new width = %#v\n\n", newWidth)
		fmt.Printf("\n\n ratio = %#v\n\n", ratio)
	} else {
		newWidth, newHeight = float64(exactWidth), float64(exactHeight)
	}

	return int(newWidth), int(newHeight)
}

func (i Image) processResize(img image.Image, width uint, height uint, destinationFileName string) {

	resized := resize.Resize(width, height, img, resize.NearestNeighbor)

	destination, err := os.Create(i.GetNamedPath(destinationFileName))
	if err != nil {
		panic(err)
	}
	defer destination.Close()

	jpeg.Encode(destination, resized, &jpeg.Options{Quality: 70})

}

func makeDir(dir string) error {
	if fileNotExists(dir) {
		if err := os.MkdirAll(dir, os.ModePerm); err != nil {
			return err
		}
	}

	return nil
}

func fileNotExists(file string) bool {
	_, err := os.Stat(file)
	return os.IsNotExist(err)
}

func fileExists(file string) bool {
	return !fileNotExists(file)
}

func mustMakeDir(dir string) {
	if err := makeDir(dir); err != nil {
		panic(err)
	}
}
